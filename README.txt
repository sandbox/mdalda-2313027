-Overview
This module is a lightweight project that allows a site administrator to hide
and restrict Paths, Forms and Tabs of Drupal Core User to certain user roles.
It's oriented to be used with other login modules.


-Features
It can disable and remove Paths, Forms and Tabs to:

    -User login
    -Reset Password
    -User registration
    -Edit profile


-Settings
Module settings are placed in Configuration -> People -> Account settings
URL: admin/config/people/accounts


-Emergency Mode
If you can't login using other alternative modules and need to reactivate
Drupal Core User system, simply add this line in the top of your index.php,
after php opening tag and you will be able to use yourdomain.com/user again.

$_disable_drupal_user_off = TRUE;
